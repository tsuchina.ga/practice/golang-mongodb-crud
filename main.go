package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tsuchina.ga/practice/golang-mongodb-crud/config"
	"gitlab.com/tsuchina.ga/practice/golang-mongodb-crud/router"
	"io"
	"log"
	"os"
	"time"
)

var conf = config.Get()

func main() {
	// router
	r := router.New()

	r.Run(":" + conf.Web.HttpPort)
}

func init() {
	// locationの設定
	location := conf.General.Timezone
	loc, _ := time.LoadLocation(location)
	time.Local = loc

	// logger
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	logPath := conf.General.LogDir
	noticeLog := logPath + conf.General.NoticeLogName
	errorLog := logPath + conf.General.ErrorLogName

	n, err := os.OpenFile(noticeLog, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
	if os.IsNotExist(err) {
		n, _ = os.Create(noticeLog)
	}
	gin.DefaultWriter = io.MultiWriter(n, os.Stdout)
	log.SetOutput(gin.DefaultWriter)

	e, err := os.OpenFile(errorLog, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
	if os.IsNotExist(err) {
		e, _ = os.Create(errorLog)
	}
	gin.DefaultErrorWriter = io.MultiWriter(e, os.Stdout)
}
