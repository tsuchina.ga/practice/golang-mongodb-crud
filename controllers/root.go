package controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Top(c *gin.Context) {
	c.HTML(http.StatusOK, "top", gin.H{"title": "こんにちわーるど"})
}

func NotFound(c *gin.Context) {
	c.HTML(http.StatusNotFound, "errors/404", gin.H{"title": "404 Not Found"})
}

func MethodNotAllowed(c *gin.Context) {
	c.HTML(http.StatusMethodNotAllowed, "errors/405", gin.H{"title": "405 Method Not Allowed"})
}
