package controllers

type shopNewPostForm struct {
	Name string `form:"name" binding:"required,min=3"`
}

func (form *shopNewPostForm) message() map[string]map[string]string {
	return map[string]map[string]string{
		"Name": {
			"required": "名称は必須です",
			"min":      "名称は3文字以上です",
		},
	}
}

type shopUpdatePostForm struct {
	Id   string `form:"id" binding:"required"`
	Name string `form:"name" binding:"required,min=3"`
}

func (form *shopUpdatePostForm) message() map[string]map[string]string {
	return map[string]map[string]string{
		"Name": {
			"required": "名称は必須です",
			"min":      "名称は3文字以上です",
		},
	}
}
