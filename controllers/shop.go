package controllers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tsuchina.ga/practice/golang-mongodb-crud/models"
	"gopkg.in/go-playground/validator.v8"
	"net/http"
)

func ShopList(c *gin.Context) {
	service := models.InjectedShopService()
	message := ""

	if deleteId := c.DefaultQuery("delete", ""); deleteId != "" {
		if err := service.Remove(deleteId); err != nil {
			message = models.GetErrorMessage(err)
		} else {
			// 成功時はリダイレクトしてgetパラメータを消しておく
			c.Redirect(http.StatusSeeOther, "/shops")
		}
	}

	shops, err := service.FindAll()
	if err != nil {
		message = models.GetErrorMessage(err)
	}

	c.HTML(http.StatusOK, "shops/list", gin.H{"title": "Shops", "shops": shops, "errors": gin.H{"message": message}})
}

func ShopNew(c *gin.Context) {
	c.HTML(http.StatusOK, "shops/new", gin.H{"title": "New Shop"})
}

func ShopNewPost(c *gin.Context) {
	form := shopNewPostForm{}
	if err := c.ShouldBind(&form); err != nil {
		if ve, ok := err.(validator.ValidationErrors); ok {
			c.HTML(http.StatusOK, "shops/new", gin.H{
				"title":  "New Shop",
				"form":   form,
				"errors": parseError(&form, ve),
			})
			return
		} else {
			c.HTML(http.StatusOK, "shops/new", gin.H{
				"title":  "New Shop",
				"errors": gin.H{"message": models.GetErrorMessage(err)},
			})
			return
		}
	}

	service := models.InjectedShopService()
	if err := service.New(form.Name); err != nil {
		c.HTML(http.StatusOK, "shops/new", gin.H{
			"title":  "New Shop",
			"errors": gin.H{"message": models.GetErrorMessage(err)},
		})
		return
	}

	c.Redirect(http.StatusSeeOther, "/shops")
}

func ShopUpdate(c *gin.Context) {
	service := models.InjectedShopService()
	message := ""

	id := c.DefaultQuery("id", "")
	shop, err := service.FindId(id)
	if err != nil {
		message = models.GetErrorMessage(err)
	}

	c.HTML(http.StatusOK, "shops/update", gin.H{"title": "Update Shop", "shop": shop, "errors": gin.H{"message": message}})
}

func ShopUpdatePost(c *gin.Context) {
	form := shopUpdatePostForm{}
	if err := c.ShouldBind(&form); err != nil {
		if ve, ok := err.(validator.ValidationErrors); ok {
			c.HTML(http.StatusOK, "shops/update", gin.H{
				"title":  "Update Shop",
				"form":   form,
				"errors": parseError(&form, ve),
			})
			return
		} else {
			c.HTML(http.StatusOK, "shops/update", gin.H{
				"title":  "Update Shop",
				"errors": gin.H{"message": models.GetErrorMessage(err)},
			})
			return
		}
	}

	service := models.InjectedShopService()
	if err := service.Update(form.Id, form.Name); err != nil {
		c.HTML(http.StatusOK, "shops/update", gin.H{
			"title":  "Update Shop",
			"errors": gin.H{"message": models.GetErrorMessage(err)},
		})
		return
	}

	c.Redirect(http.StatusSeeOther, "/shops")
}
