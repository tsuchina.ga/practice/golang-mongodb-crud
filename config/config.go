package config

import (
	"github.com/BurntSushi/toml"
	"sync"
)

var c *conf
var o sync.Once

func Get() *conf {
	o.Do(func() {
		c = new(conf)
		if _, err := toml.DecodeFile("config/config.toml", c); err != nil {
			panic(err)
		}
	})

	return c
}

type conf struct {
	General generalConf
	Web     webConf
	Db      dbConf
}

type generalConf struct {
	Timezone      string `toml:"timezone"`
	LogDir        string `toml:"logDir"`
	NoticeLogName string `toml:"noticeLogName"`
	ErrorLogName  string `toml:"errorLogName"`
}

type webConf struct {
	Host     string `toml:"host"`
	HttpPort string `toml:"httpPort"`
}

type dbConf struct {
	Address  string `toml:"address"`
	Database string `toml:"database"`
	Username string `toml:"username"`
	Password string `toml:"password"`
}
