package router

import (
	"github.com/gin-gonic/gin"
)

func static(r *gin.Engine) {
	r.StaticFile("/robots.txt", "./views/static/robots.txt")
	r.Static("/css", "./views/static/css")
	r.Static("/js", "./views/static/js")
}
