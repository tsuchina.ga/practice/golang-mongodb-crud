package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tsuchina.ga/practice/golang-mongodb-crud/controllers"
)

func web(r *gin.Engine) {
	r.GET("/", controllers.Top)

	shop := r.Group("/shops")
	{
		shop.GET("", controllers.ShopList)
		shop.GET("/new", controllers.ShopNew)
		shop.POST("/new", controllers.ShopNewPost)
		shop.GET("/update", controllers.ShopUpdate)
		shop.POST("/update", controllers.ShopUpdatePost)
	}
}
