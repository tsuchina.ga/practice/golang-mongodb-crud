package router

import (
	"github.com/foolin/gin-template"
	"github.com/gin-gonic/gin"
	"gitlab.com/tsuchina.ga/practice/golang-mongodb-crud/controllers"
	"html/template"
)

func New() *gin.Engine {
	r := gin.Default()

	r.HTMLRender = gintemplate.New(gintemplate.TemplateConfig{
		Root:         "views",
		Extension:    ".html",
		Master:       "layouts/master",
		Funcs:        template.FuncMap{},
		DisableCache: true,
	})

	static(r)
	web(r)

	r.NoRoute(controllers.NotFound)
	r.NoMethod(controllers.MethodNotAllowed)

	return r
}
