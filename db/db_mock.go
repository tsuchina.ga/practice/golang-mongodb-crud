package db

type MockSession struct{}

func (fs *MockSession) Close() {}
func (fs *MockSession) DB(name string) Database {
	return new(MockDatabase)
}

type MockDatabase struct{}

func (fd *MockDatabase) C(name string) Collection {
	return new(MockCollection)
}

type MockCollection struct{}

func (c *MockCollection) Find(interface{}) Query {
	return new(MockQuery)
}
func (c *MockCollection) FindId(interface{}) Query {
	return new(MockQuery)
}
func (c *MockCollection) Insert(...interface{}) error {
	return nil
}
func (c *MockCollection) UpdateId(interface{}, interface{}) error {
	return nil
}
func (c *MockCollection) RemoveId(interface{}) error {
	return nil
}

type MockQuery struct{}

func (q *MockQuery) All(interface{}) error {
	return nil
}
func (q *MockQuery) One(interface{}) error {
	return nil
}
