package db

import (
	"gitlab.com/tsuchina.ga/practice/golang-mongodb-crud/config"
	"gopkg.in/mgo.v2"
	"log"
	"os"
)

var session *mgo.Session
var isConnected bool

type Session interface {
	DB(string) Database
	Close()
}

type Database interface {
	C(string) Collection
}

type Collection interface {
	Find(interface{}) Query
	FindId(interface{}) Query
	Insert(...interface{}) error
	UpdateId(interface{}, interface{}) error
	RemoveId(interface{}) error
}

type Query interface {
	All(interface{}) error
	One(interface{}) error
}

type MongoSession struct {
	*mgo.Session
}

func (s MongoSession) DB(name string) Database {
	return &MongoDatabase{Database: s.Session.DB(name)}
}

type MongoDatabase struct {
	*mgo.Database
}

func (d MongoDatabase) C(name string) Collection {
	return &MongoCollection{Collection: d.Database.C(name)}
}

type MongoCollection struct {
	*mgo.Collection
}

func (c *MongoCollection) Find(query interface{}) Query {
	return MongoQuery{Query: c.Collection.Find(query)}
}
func (c *MongoCollection) FindId(query interface{}) Query {
	return MongoQuery{Query: c.Collection.FindId(query)}
}

type MongoQuery struct {
	*mgo.Query
}

func GetSession() Session {
	if !isConnected {
		isConnected = true

		conf := config.Get()

		s, err := mgo.DialWithInfo(&mgo.DialInfo{
			Addrs:    []string{conf.Db.Address},
			Database: conf.Db.Database,
			Username: conf.Db.Username,
			Password: conf.Db.Password,
		})

		if err != nil {
			log.Println(err)
			os.Exit(-1)
		}

		session = s
	}

	return MongoSession{session.Copy()}
}

func GetDatabase() Database {
	conf := config.Get()

	session := GetSession()
	return session.DB(conf.Db.Database)
}

func GetCollection(c string) Collection {
	database := GetDatabase()
	return database.C(c)
}
