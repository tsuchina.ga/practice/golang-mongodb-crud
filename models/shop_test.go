package models

import (
	"gitlab.com/tsuchina.ga/practice/golang-mongodb-crud/db"
	"gopkg.in/mgo.v2/bson"
	"testing"
)

var (
	testShopRepository ShopRepository
	testShopService    *ShopService
)

func init() {
	testShopRepository = &shopRepository{c: new(db.MockCollection)}
	testShopService = &ShopService{shopRepository: testShopRepository}
}

func TestShopRepository_All(t *testing.T) {
	_, err := testShopRepository.All()
	if err != nil {
		t.Fatalf("failed test err is '%#v'", err)
	}
}

func TestShopRepository_FindId(t *testing.T) {
	_, err := testShopRepository.All()
	if err != nil {
		t.Fatalf("failed test err is '%#v'", err)
	}
}

func TestShopRepository_New(t *testing.T) {
	err := testShopRepository.New(Shop{})
	if err != nil {
		t.Fatalf("failed test err is '%#v'", err)
	}
}

func TestShopRepository_Update(t *testing.T) {
	err := testShopRepository.Update(bson.NewObjectId(), bson.M{})
	if err != nil {
		t.Fatalf("failed test err is '%#v'", err)
	}
}

func TestShopRepository_Remove(t *testing.T) {
	err := testShopRepository.Remove(bson.NewObjectId())
	if err != nil {
		t.Fatalf("failed test err is '%#v'", err)
	}
}

func TestShopService_FindAll(t *testing.T) {
	_, err := testShopService.FindAll()
	if err != nil {
		t.Fatalf("failed test err is '%#v'", err)
	}
}

func TestShopService_FindId1(t *testing.T) {
	_, err := testShopService.FindId("5beef0a1f38b400031d3ca49")
	if err != nil {
		t.Fatalf("failed test err is '%#v'", err)
	}
}

func TestShopService_FindId2(t *testing.T) {
	_, err := testShopService.FindId("")
	if err.Error() != "not objectId" {
		t.Fatalf("'not objectId'のerrorが返されませんでした")
	}
}

func TestShopService_New(t *testing.T) {
	err := testShopService.New("test")
	if err != nil {
		t.Fatalf("failed test err is '%#v'", err)
	}
}

func TestShopService_Update1(t *testing.T) {
	err := testShopService.Update("5beef0a1f38b400031d3ca49", "test")
	if err != nil {
		t.Fatalf("failed test err is '%#v'", err)
	}
}

func TestShopService_Update2(t *testing.T) {
	err := testShopService.Update("", "test")
	if err.Error() != "not objectId" {
		t.Fatalf("'not objectId'のerrorが返されませんでした")
	}
}

func TestShopService_Remove1(t *testing.T) {
	err := testShopService.Remove("5beef0a1f38b400031d3ca49")
	if err != nil {
		t.Fatalf("failed test err is '%#v'", err)
	}
}

func TestShopService_Remove2(t *testing.T) {
	err := testShopService.Remove("")
	if err.Error() != "not objectId" {
		t.Fatalf("'not objectId'のerrorが返されませんでした")
	}
}
