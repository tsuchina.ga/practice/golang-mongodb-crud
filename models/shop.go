package models

import (
	"gitlab.com/tsuchina.ga/practice/golang-mongodb-crud/db"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Shop struct {
	Id        bson.ObjectId `bson:"_id" json:"id"`
	Name      string        `bson:"name" json:"name"`
	CreatedAt time.Time     `bson:"created" json:"created"`
	UpdatedAt time.Time     `bson:"updated" json:"updated"`
}

func InjectedShopRepository() ShopRepository {
	return NewShopRepository(db.GetCollection("shops"))
}

func InjectedShopService() *ShopService {
	return NewShopService(InjectedShopRepository())
}

type ShopRepository interface {
	All() ([]Shop, error)
	FindId(bson.ObjectId) (Shop, error)
	New(Shop) error
	Update(bson.ObjectId, bson.M) error
	Remove(bson.ObjectId) error
}

func NewShopRepository(c db.Collection) ShopRepository {
	return &shopRepository{
		c: c,
	}
}

type shopRepository struct {
	c db.Collection
}

func (r *shopRepository) All() (shops []Shop, err error) {
	err = r.c.Find(bson.M{}).All(&shops)
	return
}

func (r *shopRepository) FindId(id bson.ObjectId) (shop Shop, err error) {
	err = r.c.FindId(id).One(&shop)
	return
}

func (r *shopRepository) New(shop Shop) error {
	return r.c.Insert(shop)
}

func (r *shopRepository) Update(id bson.ObjectId, params bson.M) error {
	return r.c.UpdateId(id, params)
}

func (r *shopRepository) Remove(id bson.ObjectId) error {
	return r.c.RemoveId(id)
}

func NewShopService(repo ShopRepository) *ShopService {
	return &ShopService{
		shopRepository: repo,
	}
}

type ShopService struct {
	shopRepository ShopRepository
}

func (s *ShopService) FindAll() (shops []Shop, err error) {
	return s.shopRepository.All()
}

func (s *ShopService) FindId(id string) (shop Shop, err error) {
	if !bson.IsObjectIdHex(id) {
		err = ErrNotObjectIdHex
		return
	}

	return s.shopRepository.FindId(bson.ObjectIdHex(id))
}

func (s *ShopService) New(name string) error {
	shop := Shop{
		Id:        bson.NewObjectId(),
		Name:      name,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	return s.shopRepository.New(shop)
}

func (s *ShopService) Update(id, name string) error {
	if !bson.IsObjectIdHex(id) {
		return ErrNotObjectIdHex
	}

	return s.shopRepository.Update(bson.ObjectIdHex(id), bson.M{"name": name})
}

func (s *ShopService) Remove(id string) error {
	if !bson.IsObjectIdHex(id) {
		return ErrNotObjectIdHex
	}

	return s.shopRepository.Remove(bson.ObjectIdHex(id))
}
