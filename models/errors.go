package models

import (
	"errors"
	"gopkg.in/mgo.v2"
)

var (
	ErrNotFound       = mgo.ErrNotFound
	ErrNotObjectIdHex = errors.New("not objectId hex")
)

func GetErrorMessage(err error) string {
	switch err {
	case ErrNotFound:
		return "指定されたデータが見つかりません"
	case ErrNotObjectIdHex:
		return "指定されたIDの形式に誤りがあります"
	}
	return err.Error()
}
